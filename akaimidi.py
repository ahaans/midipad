import mido

iport = mido.open_input('APC MINI 0')
port = mido.open_output('APC MINI 1')

OFF = 0
GREEN = 1
GREEN_B = 2
RED = 3
RED_B = 4
YELLOW = 5
YELLOW_B = 6

def clear_lights():
    for i in range(72):
        msg = mido.Message('note_on', note=i, velocity=OFF)
        port.send(msg);
    for i in range(8):
        msg = mido.Message('note_on', note=82+i, velocity=OFF)
        port.send(msg);

clear_lights();
